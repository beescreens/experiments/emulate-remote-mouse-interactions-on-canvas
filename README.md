# Emulate remote mouse interactions on canvas

Demonstration on how to emulate remote mouse interactions on HTML5 canvas with WebSockets.

## Installation

```sh
npm install
```

## Run the app

```sh
npm run start:dev
```

You can now access <http://localhost:3000> in two separate windows to interact with the remote canvas.
