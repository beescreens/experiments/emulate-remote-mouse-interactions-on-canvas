const socket = io();

const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d');

const size = 500;
canvas.style.width = `${size}px`;
canvas.style.height = `${size}px`;

const scale = window.devicePixelRatio;

canvas.width = Math.floor(size * scale);
canvas.height = Math.floor(size * scale);

context.scale(scale, scale);

socket.on('POINT_START', (point) => {
  const event = new MouseEvent('mousedown', {
    clientX: point.x,
    clientY: point.y,
  });

  canvas.dispatchEvent(event);
});

socket.on('POINT_MOVE', (point) => {
  const event = new MouseEvent('mousemove', {
    clientX: point.x,
    clientY: point.y,
  });

  canvas.dispatchEvent(event);
});

socket.on('POINT_END', (point) => {
  const event = new MouseEvent('mouseup', {
    clientX: point.x,
    clientY: point.y,
  });

  canvas.dispatchEvent(event);
});

function getMousePos(canvas, event) {
  const rect = canvas.getBoundingClientRect();

  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top,
  };
}

function onmousedownHandler(event) {
  const pos = getMousePos(canvas, event);

  if (event.isTrusted) {
    socket.emit('POINT_START', {
      x: pos.x,
      y: pos.y,
    });
  }

  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillStyle = '#abc123';
  context.font = 'bold 12px';
  context.fillText(`[${pos.x}, ${pos.y}]`, pos.x, pos.y);
}

function onmousemoveHandler(event) {
  const pos = getMousePos(canvas, event);

  if (event.isTrusted) {
    socket.emit('POINT_MOVE', {
      x: pos.x,
      y: pos.y,
    });
  }

  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillStyle = '#1a2b3c';
  context.font = 'bold 12px';
  context.fillText(`[${pos.x}, ${pos.y}]`, pos.x, pos.y);
}

function onmouseupHandler(event) {
  const pos = getMousePos(canvas, event);

  if (event.isTrusted) {
    socket.emit('POINT_END', {
      x: pos.x,
      y: pos.y,
    });
  }
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillStyle = '#123abc';
  context.font = 'bold 12px';
  context.fillText(`[${pos.x}, ${pos.y}]`, pos.x, pos.y);
}

canvas.addEventListener('mousedown', onmousedownHandler);
canvas.addEventListener('mousemove', onmousemoveHandler);
canvas.addEventListener('mouseup', onmouseupHandler);
